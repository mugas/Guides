<h1>GraphQl</h1>

<h4>Definition</h4>

Graphql is a text-based language specification created by Facebook for client-server communication.
It lets you model the resources and process provided by a server as a domain-specific language(DSL).

