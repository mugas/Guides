<h1>The Code Guide</h1>

<h4>A guide for all code Newbies, but that everybody can help</h4>

Everybody learns at different pace, from different places in different ways.
Some of us, learn from courses online, some free others paid. Some learn by their own, others go to university. Others can learn only after work, other all day.
What might be important to me in coding, might not be to you. What I think is difficult in coding, might not be for you. So why we don´t get together and share all our knowledge?

Also, when start learning web development, the jargon and the explanations also can be difficult to acknowledge in the begginning.

So the idea is that, every one that is learning can add their own touch and info and we will get a valuable <em>Codepedia</em> of code that can be use by all.

Can be about anything and everything related to code. My focus will be more about <strong>JS</strong> and <strong>HTML</strong> and </strong>CSS</strong> and also <strong>Node.js</strong> and <strong>Vue.js</strong> but you can write about <strong>Python</strong>, <strong>Ruby</strong> or the same subject as me. The more the merrier.

It is said that <em>we learn better when we teach it, because we learn it twice</em>
So let´s do that.

Some small ground settings to be equal:

<li>I have some files in JS, so let´s say you create something in Ruby, create the folder first</li>

<li>To be easier to all divide, each file by subject. Example in js: arrays, objects, functions, etc</li>

<li>I haven´t put yet but any good links or explanations you find about the subject maybe put in the end of the file of each subject</li>

</li>Before add any info, check if the info is not there already and if so, add it in a way that makes sense</li>

This is just the beggining, so this readme is going to be improved and feel free to do so if you feel necessary with something important.

The idea is that all the tutorials and info written here would be transfered for the[CodePedia app](https://github.com/mugas/codepedia)

1)[Arrays](Javascript/Arrays.md)

2)[Basics](Javascript/Basics.md)

3)[Closures](Javscript/Closured.md)